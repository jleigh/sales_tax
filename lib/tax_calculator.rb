require_relative './parser'
require_relative './taxer'
require_relative './item'
require_relative './receipt'

class TaxCalculator
  attr_reader :parser, :taxer, :receipt
  INVALID_INPUT_ERROR = "InvalidInputError: Input must be a list of strings"
  INVALID_ITEM_ERROR = "InvalidInputError: Item must be a string"
  def initialize(parser, taxer)
    @parser = parser
    @taxer = taxer
  end

  def calc(input)
    raise INVALID_INPUT_ERROR if invalid_input?(input)

    items = input.map do |item|
      raise INVALID_ITEM_ERROR if invalid_item?(item)
      parsed_item = parser.parse(item, Item.new)
      taxer.apply_taxes(parsed_item)
    end
    @receipt = Receipt.new(items)
    receipt.print
  end

  private
  def invalid_input?(input)
    input.class != Array
  end

  def invalid_item?(item)
    item.class != String
  end
end

input = [
  "1 book at 12.49",
  "1 music CD at 14.99", 
  "1 imported bottle of perfume at 47.50", 
  "2 packets of headache pills at 9.75",
  "3 toothbrush 7.99"
]
calculator = TaxCalculator.new(Parser, Taxer)
calculator.calc(input)