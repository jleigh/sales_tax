class Parser

  ITEM_LOOKUP = {
    book: :book,
    chocolate: :food,
    pills: :medicine
  }

  PRICE_REGEX = %r{\d*[.]\d{2}}
  VALIDATOR_REGEX = %r{[a-zA-Z]}
  COLON_INSERT_REGEX = %r{[a-zA-Z](\s)\d}
  INVALID_INPUT_ERROR = "InvalidInputError:"

  def self.parse(input, item)
    item = parse_import(input, item)
    item = parse_price(input, item)
    item = parse_quantity(input, item)
    item = parse_type(input, item)
    format_output(input, item)
  end

  private
  def self.parse_import(input, item)
    item.imported = true if imported?(input)
    item
  end

  def self.parse_price(input, item)
    raise INVALID_INPUT_ERROR + 'Price missing' if price_absent?(input)
    
    item.price = input.match(PRICE_REGEX)[0].to_f
    item
  end

  def self.parse_quantity(input, item)
    raise INVALID_INPUT_ERROR + 'Quantity missing' if quantity_absent?(input)

    item.quantity = input.split[0].to_i
    item
  end

  def self.parse_type(input, item)
    raise INVALID_INPUT_ERROR  + 'Item missing' if item_absent?(input)

    key = ITEM_LOOKUP.keys.find { |key| input.match(key.to_s) }.to_s.to_sym
    type = ITEM_LOOKUP[key]
    item.type = ITEM_LOOKUP[key] || :other
    item
  end

  def self.format_output(input, item)
    output = insert_colon(input)
    output = format_imported(output) if item.imported
    item.output = output
    item
  end

  def self.format_imported(input)
    output = input.gsub('imported ', '')
    output.split.insert(1, 'imported').join(' ')
  end

  def self.insert_colon(input)
    new_input = input.gsub(' at', '')
    match = new_input.match(COLON_INSERT_REGEX)
    index = match.begin(1)
    new_input.insert(index, ':')
  end

  def self.imported?(input)
    input.match('imported')&.size == 1
  end

  def self.price_absent?(input)
    input.match(PRICE_REGEX).nil?
  end

  def self.quantity_absent?(input)
    quantity = input.split[0].to_i
    quantity < 1
  end

  def self.item_absent?(input)
    input.match(VALIDATOR_REGEX).nil?
  end
end