require 'spec_helper'
require 'ostruct'

describe Receipt do
  let(:item1) { OpenStruct.new(type: :book, price: 12.50, quantity: 1, imported: true, import_tax: 0.63, total_taxes: 0.63) }
  let(:item2) { OpenStruct.new(type: :other, price: 2.50, quantity: 2, sales_tax: 0.5, import_tax: 0.25, imported: true, total_taxes: 0.75) }
  let(:receipt) { Receipt.new([item1, item2]) }

  describe '#initialize' do
    it 'should set the total and total taxes from all the items' do
      expect(receipt.items).to eq([item1, item2])
      expect(receipt.total).to eq(15.0)
      expect(receipt.total_taxes).to eq(1.38)
    end
  end
end