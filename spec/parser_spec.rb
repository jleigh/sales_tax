require 'spec_helper'
require 'ostruct'

describe Parser do
  describe '.parse' do
    context 'with valid input' do
      let(:item1) { OpenStruct.new(price: 12.49, quantity: 1, type: :book, output: '1 book: 12.49') }
      let(:item2) { OpenStruct.new(imported: true, price: 12.49, quantity: 1, type: :food, output: '1 imported box of chocolates: 12.49') }
      let(:item3) { OpenStruct.new(price: 14.99, quantity: 1, type: :other, output: '1 music CD: 14.99') }
      let(:item4) { OpenStruct.new(price: 3.99, quantity: 2, type: :other, output: '2 toothbrush: 3.99') }

      it 'returns an object with attributes from the input' do
        expect(Parser.parse("1 book at 12.49", OpenStruct.new)).to eq(item1)
        expect(Parser.parse("1 imported box of chocolates at 12.49", OpenStruct.new)).to eq(item2)
        expect(Parser.parse("1 box of imported chocolates at 12.49", OpenStruct.new)).to eq(item2)
        expect(Parser.parse("2 toothbrush 3.99", OpenStruct.new)).to eq(item4)
      end
    end
   
    context 'with invalid input' do
      let(:input1) { "book" }
      let(:input2) { "book 12.99" }
      let(:input3) { "1 book" }
      let(:input4) { "1 12.99" }

      it 'raises an error' do
        expect{ Parser.parse(input1, OpenStruct.new) }.to raise_error(Parser::INVALID_INPUT_ERROR + "Price missing")
        expect{ Parser.parse(input2, OpenStruct.new) }.to raise_error(Parser::INVALID_INPUT_ERROR + "Quantity missing")
        expect{ Parser.parse(input3, OpenStruct.new) }.to raise_error(Parser::INVALID_INPUT_ERROR + "Price missing")
        expect{ Parser.parse(input4, OpenStruct.new) }.to raise_error(Parser::INVALID_INPUT_ERROR + "Item missing")
      end
    end
  end
end
# # handle multiple exemptions