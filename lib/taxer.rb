class Taxer
  UNTAXABLE_ITEMS = [:book, :food, :medicine]
  TAX_TYPES = ['import', 'sales']
  SALES_TAX = 0.1
  IMPORT_TAX = 0.05

  def self.apply_taxes(item)
    item = set_tax('import', imported?(item), item)
    item = set_tax('sales', taxable?(item), item)
    item = set_total_taxes(item)
  end

  private

  def self.set_tax(type, taxed, item)
    constant = const_get("#{type.upcase}_TAX")
    result = taxed ? calc(constant, item) : 0
    item.send("#{type}_tax=", result)
    item
  end

  def self.imported?(item)
    item.imported
  end

  def self.taxable?(item)
    !UNTAXABLE_ITEMS.include?(item.type)
  end

  def self.calc(tax, item)
    tax * item.price * item.quantity
  end

  def self.set_total_taxes(item)
    item.total_taxes = item.import_tax + item.sales_tax
    item
  end
end