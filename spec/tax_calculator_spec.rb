require 'spec_helper'

describe 'TaxCalculator' do
  describe "#calc" do
    let(:calculator) { TaxCalculator.new(Parser, Taxer) }
    context "with valid input" do
       it 'should return a receipt with the output' do
        input = ["1 book at 12.49", "1 music CD at 14.99", "1 imported bottle of perfume at 47.50", "2 packets of headache pills at 9.75"]
        calculator.calc(input)
        expect(calculator.receipt.total).to eq(84.73)
        expect(calculator.receipt.total_taxes).to eq(8.624)
      end
    end

    context "with invalid input" do
      context "with invalid item in input" do
        it 'should raise an error' do
          input1 = ["1 book at 12.49", 9]
          input2 = [true]
          input3 = [[]]
          expect{ calculator.calc(input1) }.to raise_error(TaxCalculator::INVALID_ITEM_ERROR)
          expect{ calculator.calc(input2) }.to raise_error(TaxCalculator::INVALID_ITEM_ERROR)
          expect{ calculator.calc(input3) }.to raise_error(TaxCalculator::INVALID_ITEM_ERROR)
        end
      end

      it 'should raise an error' do
        input = "1 book at 12.49"
        expect{ calculator.calc(input) }.to raise_error(TaxCalculator::INVALID_INPUT_ERROR)
      end

      it 'should raise an error' do
        input = 9
        expect{ calculator.calc(input) }.to raise_error(TaxCalculator::INVALID_INPUT_ERROR)
      end

      it 'should raise an error' do
        input = true
        expect{ calculator.calc(input) }.to raise_error(TaxCalculator::INVALID_INPUT_ERROR)
      end
    end
  end
end