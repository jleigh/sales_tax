class Receipt
  attr_accessor :items, :total, :total_taxes

  def initialize(items)
    @items = items
    @total = 0
    @total_taxes = 0
    build_receipt
  end

  def print
    items.each { |item| puts item.output }
    puts "Sales taxes: #{total_taxes}"
    puts "Total: #{total}"
    puts '=================================='
  end

  private

  def build_receipt
    items.each do |item|
      @total += item.price
      @total_taxes += item.total_taxes
    end
  end
end