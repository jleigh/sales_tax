## Problem

Basic sales tax is applicable at a rate of 10% on all goods, except food, medicines and books. For all imported goods, a tax rate of additional 5% is applied irrespective of their type. Generate a receipt which shows names of all the items purchased and their prices (including taxes), finishing with total cost of the items, and total amount sales tax paid. The rounding rules for sales tax are that for a tax rate of n%, a shelf price of P contains(np/100 rounded up to nearest 0.05) amount of sales tax. Write an application to generate receipt with all these details.

#Input:

* 1 imported bottle of perfume at 27.99
* 1 bottle of perfume at 18.99
* 1 packet of headache pills at 9.75
* 1 box of imported chocolates at 11.25

#Output:

* 1 imported bottle of perfume: 32.19
* 1 bottle of perfume: 20.89
* 1 packet of headache pills: 9.75
* 1 imported box of chocolates: 11.85

Sales Taxes: 6.70
Total: 74.68