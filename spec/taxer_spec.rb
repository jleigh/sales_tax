require 'spec_helper'

describe Taxer do
  describe '.apply_taxes' do
    context 'when item is not taxable' do
      it 'should not tax fields' do
        item = OpenStruct.new(type: :book, price: 12.50, quantity: 1)
        Taxer.apply_taxes(item)
        expect(item.import_tax).to eq(0)
        expect(item.sales_tax).to eq(0)
        expect(item.total_taxes).to eq(0)
      end
    end

    context 'when item is basic taxable' do
      it 'should set the basic tax field and taxable to true' do
        item = OpenStruct.new(type: :other, price: 12.50, quantity: 1)
        Taxer.apply_taxes(item)
        expect(item.sales_tax).to eq(Taxer::SALES_TAX * item.price)
        expect(item.import_tax).to eq(0)
        expect(item.total_taxes).to eq(1.25)
      end
    end

    context 'when item is import taxable' do
      it 'should set the import tax field' do
        item = OpenStruct.new(type: :medicine, price: 12.50, quantity: 1, imported: true)
        Taxer.apply_taxes(item)
        expect(item.import_tax).to eq(Taxer::IMPORT_TAX * item.price)
        expect(item.sales_tax).to eq(0)
        expect(item.total_taxes).to eq(0.625)
      end
    end

    context 'when item is both basic and import taxable' do
      it 'should set the basic tax field and import tax field' do
        item = OpenStruct.new(type: :other, price: 12.50, quantity: 1, imported: true)
        Taxer.apply_taxes(item)
        expect(item.import_tax).to eq(Taxer::IMPORT_TAX * item.price)
        expect(item.sales_tax).to eq(Taxer::SALES_TAX * item.price)
        expect(item.total_taxes).to eq(1.875)
      end
    end
  end
end